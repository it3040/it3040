#include <bits/stdc++.h>
using namespace std;

int keys[1000005];
fstream fileInPut;
int key, n;

void menu();
void InPut();
int nhap_key();
void test();
int tim_kiem_tuan_tu(int key);
int tim_kiem_nhi_phan1(int key); ///cai dat voi vong lap
int tim_kiem_nhi_phan2(int key, int l, int r); ///cai dat voi de quy

int main()
{
//    ios_base::sync_with_stdio(0);
    string f1;
    int choice;
    char ch;

    do {
        menu();
        cout << " ban chon: ";
        cin >> choice;
        switch(choice) {
        case 1: {
            f1 = "data/input1.txt";
            n = 10;
            break;
        }
        case 2: {
            f1 = "data/input2.txt";
            n = 100;
            break;
        }
        case 3: {
            f1 = "data/input3.txt";
            n = 1000;
            break;
        }
        case 4: {
            f1 = "data/input4.txt";
            n = 10000;
            break;
        }
        case 5: {
            f1 = "data/input5.txt";
            n = 100000;
            break;
        }
        case 6: {
            f1 = "data/input6.txt";
            n = 1000000;
            break;
        }
        case 7:
            return 0;
        }
        fileInPut.open(&f1[0], ios::in);
        if(fileInPut.fail()) {
            cout << " khong mo duoc " << endl;
            exit(1);
        }
        InPut();
        nhap_key();
        test();
        fileInPut.close();
        cout << " ban co muon tiep tuc(y/n): ";
        cin >> ch;
        cout << "\n\n";
    } while (ch == 'y' || ch == 'Y');
    return 0;
}

void menu()
{
    cout << " chon file input:\n"
         << " 1. input1 (day 10 phan tu)\n"
         << " 2. input2 (day 100 phan tu)\n"
         << " 3. input3 (day 1000 phan tu)\n"
         << " 4. input4 (day 10000 phan tu)\n"
         << " 5. input5 (day 100000 phan tu)\n"
         << " 6. input6 (day 1000000 phan tu)\n"
         << " 7. thoat\n";
}
void InPut()
{
    int index = 0;
    while (!fileInPut.eof()) {
        fileInPut >> keys[index];
        index++;
    }
}
int nhap_key()
{
    cout << " nhap so can tim: ";
    cin >> key;
    return key;
}

void test()
{
    double start;

    start = clock();
    cout << " vi tri: " << tim_kiem_tuan_tu(key);
    cout << "; thoi gian chay: ";
    printf("%.10lf\n", (double)(clock() - start)/CLOCKS_PER_SEC);

    start = clock();
    cout << " vi tri: " << tim_kiem_nhi_phan1(key);
    cout << "; thoi gian chay: ";
    printf("%.10lf\n", (double)(clock() - start)/CLOCKS_PER_SEC);

    start = clock();
    cout << " vi tri: " << tim_kiem_nhi_phan2(key, 0, n);
    cout << "; thoi gian chay: ";
    printf("%.10lf\n", (double)(clock() - start)/CLOCKS_PER_SEC);

    cout << " chu y: vi tri la -1 nghia la so do ko co trong day!\n";
}

int tim_kiem_tuan_tu(int key)
{
    for (int i = 0; i < n; i++) {
        if (keys[i] == key)
            return i + 1;
    }
    return -1;
}

int tim_kiem_nhi_phan1(int key)
{
    int l = 0;
    int r = n - 1;
    while (l <= r ) {
        int m = (l + r) / 2;
        if (keys[m] == key)
            return m + 1;
        else if (keys[m] > key) {
            r = m - 1;
        } else
            l = m + 1;
    }
    return -1;
}

int tim_kiem_nhi_phan2(int key, int l, int r)
{
    int m = (l + r) / 2;
    if (keys[m] == key)
        return m + 1;
    if (l > r)
        return -1;
    else if (keys[m] > key)
        return tim_kiem_nhi_phan2(key, l, m - 1);
    else
        return tim_kiem_nhi_phan2(key, m + 1, r);
}
