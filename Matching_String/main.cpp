#include <bits/stdc++.h>
#include "kmp.h"
#include "timkiemtuantu.h"

using namespace std;

#define len(a) strlen(a)/sizeof(a[0])

char text[1000005];
char pattern[100];
fstream fileInPut;

void menu();
void InPut();


int main()
{
    string f1;
    int choice;
    char ch;

    do{
        menu();
        cout << " ban chon: ";
        cin >> choice;
        switch(choice)
        {
            case 1:
            {
                f1 = "data/input1.txt";
                break;
            }
            case 2:
            {
                f1 = "data/input2.txt";
                break;
            }
            case 3:
            {
                f1 = "data/input3.txt";
                break;
            }
            case 4:
            {
                f1 = "data/input4.txt";
                break;
            }
            case 5:
            {
                f1 = "data/input5.txt";
                break;
            }
            case 6:
            {
                f1 = "data/input6.txt";
                break;
            }
            case 7: return 0;
        }

        fileInPut.open(&f1[0], ios::in);
        if(fileInPut.fail()){
            cout << " khong mo duoc " << endl;
            exit(1);
        }
        InPut();
        cout << "xau can tim(xau it hon 100 ki tu): ";
        fflush(stdin);
        gets(pattern);
        fileInPut.close();

        cout << "\n1.tim kiem tuan tu";
        timKiemTuanTu(text, pattern);

        cout << "\n2.tim kiem bang thuat toan KMP";
        int prefix[len(pattern)];
        computePrefix(pattern, prefix);
        KMP(text, pattern, prefix);

        cout << "\n ban co muon tiep tuc(y/n): ";
        cin >> ch;
        cout << "\n\n";
    }while (ch == 'y' || ch == 'Y');
    return 0;
}

void menu()
{
    cout << " chon file input:\n"
         << " 1. input1 (xau 10 ki tu)\n"
         << " 2. input2 (xau 100 ki tu)\n"
         << " 3. input3 (xau 1000 ki tu)\n"
         << " 4. input4 (xau 10000 ki tu)\n"
         << " 5. input5 (xau 100000 ki tu)\n"
         << " 6. input6 (xau 1000000 ki tu)\n"
         << " 7. thoat\n";
}

void InPut()
{
    int index = 0;
    while (!fileInPut.eof()){
        fileInPut >> text[index];
        index++;
    }
}



