#include <bits/stdc++.h>
#include "kmp.h"

using namespace std;

#define len(a) strlen(a)/sizeof(a[0])

int* computePrefix(char pattern[], int prefix[])
{
    int m = len(pattern);
    prefix[0] = -1;
    int i = -1;
    for (int j = 1; j < m; j++){
        while ((i > -1) && (pattern[j] != pattern[i + 1])){
            i = prefix[i];
        }
        if (pattern[j] == pattern[i + 1]){
            i++;
        }
        prefix[j] = i;
    }
    return prefix;
}

void KMP(char text[], char pattern[],int prefix[])
{
    int m = len(pattern);
    int n = len(text);
    int i, j;
    i = -1;
    for (j = 0; j < n; j++){
        while ((i > -1) && (pattern[i + 1] != text[j])){
            i = prefix[i];
        }
        if (pattern[ i+ 1] == text[j]){
            i++;
        }
        if (i == m - 1){
            cout << "\nchuoi xuat hien o vi tri: " << (j - m + 1) << " ";
            i = prefix[i];
        }
    }
}
